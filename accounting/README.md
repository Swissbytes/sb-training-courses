# Accounting

- Teacher: Lic. Wilson Caba 
- Reference: [Course page](http://wilsoncaba.jimdo.com/taller-contable)

## 2015-05-18 Introducción a la contabilidad 
El programa que se presenta tiene las partes fundamentales del Ciclo Contable, 
su contenido esta sujeto a modificaciones requeridas y que tenga como objetivo 
la mayor utilidad y aplicacion en las transacciones diarias.

  -  [PROGRAMA DE CAPACITACION EN CONTABILIDAD - PDF](accounting/PROGRAMA+DE+CAPACITACION+EN+CONTABILIDAD.pdf)
  -  [Tema 1: El ciclo contable - PDF](accounting/PRESENTACION%2BCONTABILIDAD.pdf)
  -  [Plan general de cuentas](accounting/plan_de_cuentas_ind.pdf)
  -  [Ejemplo estados finacieros - XLS](accounting/estados+financieros+integrados.xls)

## 2015-05-23 Práctica contable

Se realizó una práctica completa de asientos con un ejemplo presentado.

  -  [Practica contable - PDF](accounting/Practica+Contable.pdf)
  -  [Practica contable - XLS](accounting/Practica+contable+resuelto_0.xls)
  -  [Practica contable resuelto - XLS](accounting/Practica+contable+resuelto_1.xls)

### Actividad durante la semana:
  -  Aplicar el ejercicio resuelto en WARA

## 2015-05-30 Práctica contable II

Se realizó una práctica de multiples cuentas en la hoja
Excel y en WARA.

  -  [Practica contable con cuentas resuelto - XLS](accounting/Practica+contable+resuelto_cuentas.xls)

### Actividad durante la semana:
  - Comparar el ejercicio resuelto con el que se fue desarrollando en clase

## 2015-06-06 Práctica contable III

Práctica con WARA, no se pudo completar todo pero se vieron todos los
elementos necesarios.

  - [Practica contable II - PDF](accounting/Practica+Contable+II.pdf)
  - [Practica contable a resolver - XLS](accounting/Practica+contable+a+resolver.xls)
