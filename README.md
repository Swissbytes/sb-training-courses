# Training Courses

Each course is defined by the following constraints/guidelines:

- 3-4 hours per course 
- 30% theory, 70% practice 
- Courses material will be stored in a shared repository using its code as reference
- Marks for each course will be created as wiki entries in its repository

## Courses

Course                                                              | Teacher                | Schedule
---                                                                 | ---                    | ---                                     | ---
[Accounting](accounting/.)                                          | Lic. Wilson Caba       | Saturdays 9-12am, started at 2015-05-18
[Client communication](client_communication/.)                      | Marius                 |
[Project management, fundamentals](project_survival_tips/.)         | Timoteo                | 2015-07-07
[Project management, requirements](project_requirements/.)          | Timoteo/Tobias         | 2015-07-28
[Project management, phases](project_phases/.)                      | Timoteo                | 2015-06-16
[Project management, common mistakes](problem_solving/.)            | Timoteo/Tobias         |
[Project management, things that get forgotten](problem_solving/.)  | Timoteo/Tobias         |
[Problem solving](problem_solving/.)                                | Tobias                 | 2015-06-10
[Project phases](project_phases/.)                                  | Timoteo                |
[Scrum](scrum/.)                                                    | Timoteo/Tobias         | 2015-06-23
[Personal time management](time_management/.)                       | Timoteo                | 2015-09-15 
[REST services](rest_services/.)                                    | Jorge/Eliana           |
[Estimations and Predictions](estimating/.)                         | Dahir                  | 2015-07-02
[Build automation/Continuous Integration](continuous_integration/.) | Timoteo                |
[Self-contained projects](self_contained_projects/.)                | Timoteo                |
[Feature branch development](feature_branch/.)                      | Timoteo                |
[Clean code](clean_code/.)                                          | Timoteo, Tobias, Jorge |
[GRASP Patterns](grasp_patterns/.)                                  | Jorge                  | 2015-07-24
[Continuous deployment](continuous_deployment/.)                    | Timoteo, Tobias        |
[Single page JS apps](single_page_js_apps/.)                        | Jorge                  |
[Project progress time-tracking](progress_time_tracking/.)          | Tobias                 |
[Securing REST services](securing_rest/.)                           | Jorge                  |
Testing Mindset                                                     |                        |
Dummy & Functional testing                                          |                        |
Automated testing                                                   |                        |
Exploratory testing                                                 |                        |
Security testing                                                    |                        |
Data types on Java                                                  | Timoteo/Tobias         |
Self-tested applications                                            | Timoteo                |
[Functional Programming](functional_programming/slides.pdf)         | Timoteo                | 2015-08-25
[Encoding and timezone](encoding_timezone/slides.pdf)               | Timoteo                | 2015-11-17

