# Problem Solving Techniques

A collection of 12 points to give a high level direction to how to go about solving a problem / unknown / new situation or task.

These techniques don't only apply to software development, but apply to anything.

## 2015-06-10 Presentation
- [Presentation - PDF](ProblemSolvingTechniques.pdf)
- [Audio recording - MP3](Problem_solving.mp3)
