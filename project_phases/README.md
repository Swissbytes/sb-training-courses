# Project phases

- Teacher: Timoteo Ponce

## Contents
### Inception
  - Requirements spec/Backlog
  - Contractual definition: Fixed price, hourly wage
  - Commitments
  - Team organization (2-7)
    - Team leader:
    - Developer: 
  - Definition of communication channels
    - With the customer : email, shared docs
    - With the team: skype, email
  - Definition of a schedule
    - Milestones and delivery dates
    - Packages and intermediate versions
### Setup
  - Definition of a process
    - Shared backlog
    - Iteration plan
  - Process tooling
    - Progress management
    - Revision control system
    - Build automation/continuous build
    - Issue tracker
  - Development setup
    - Centralized documentation
    - Architecture
    - Coding policies/conventions (Definition of DONE)
### Development/Testing
  - Features implementation on iterations
  - Testing of finished implementations
  - Keep a changelog
  - Integration environment
  - How to install
### Delivery
  - Operations manual
  - Installation packages
