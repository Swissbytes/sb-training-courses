import java.util.*;
import java.text.*;

public class Timezone1{

  public static void main (String[] args) throws Exception{
    SimpleDateFormat parser = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
    SimpleDateFormat fmt = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
    //fmt.setTimeZone(TimeZone.getTimeZone("America/New_York"));
    Date date = parser.parse("22-01-2015 10:15:55 AM");
    System.out.printf("'22-01-2015 10:15:55 AM' is at '%s'\n", fmt.format(date));
  }
}
