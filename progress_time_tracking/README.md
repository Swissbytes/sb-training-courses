# Accounting

- Teacher: Lic. Wilson Caba 
- Reference: [Course page](http://wilsoncaba.jimdo.com/taller-contable)

Date | Subject | Files
--- | --- | ---
2015-05-18 | Introducción a la contabilidad | [PDF:Programa](#),[PDF:Tema 1](#), [XLS:Ejemplo estados financieros](#),[PDF:Plan general de cuentas](#)
